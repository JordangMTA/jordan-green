----Client----
function createGUI (thePlayer)
	thePlayer = getLocalPlayer()
	window = guiCreateWindow(521, 152, 533, 150, "Question 9", false)
	engine = guiCreateButton(34, 47, 215, 67, "Engine On/Off", false, window)
	addEventHandler( "onClientGUIClick", engine, function() triggerServerEvent ( "changeEngineStatus", root, thePlayer ) end, false )
	lock = guiCreateButton(283, 47, 215, 67, "Doors Lock/Unlock", false, window)  
	addEventHandler( "onClientGUIClick", lock, function() triggerServerEvent ( "changeLockStatus", root, thePlayer ) end, false )	
	guiSetVisible (window, false)		
end
addEventHandler("onClientResourceStart",getResourceRootElement(getThisResource()),createGUI)

function GUI(thePlayer)
local vehicle = getPedOccupiedVehicle (thePlayer)
	if not isPedInVehicle ( thePlayer ) then
		outputChatBox ("You must be in a vehicle to use this button.")
	else	
		guiSetVisible(window, not guiGetVisible ( window ) )
		showCursor ( not isCursorShowing ( ) )
	end
end
addCommandHandler("carcontrols", GUI)


----Server----
function lock (thePlayer)
	local vehicle = getPedOccupiedVehicle(thePlayer)
	 if ( vehicle ) then                                
       if isVehicleLocked ( vehicle ) then           
            setVehicleLocked ( vehicle, false ) 
			outputChatBox("Your vehicle is now unlocked.")
	else
            outputChatBox("Your vehicle is now locked.")
            setVehicleLocked ( vehicle, true )
    end
end	
addEvent("changeLockStatus", true)
addEventHandler( "changeLockStatus", resourceRoot, lock )

function engine (thePlayer)
	local vehicle = getPedOccupiedVehicle(thePlayer)
	if ( vehicle ) then
		if isGetVehicleEngineState ( vehicle, false) then
			setVehicleEngineState (vehicle, true)
			outputChatBox ("Your vehicle is now on.")
	else
			setVehicleEngineState (vehicle, false)
			outputChatBox("Your vehicle is now off.")
		end
	end
end
addEvent("changeEngineStatus", true)
addEventHandler( "changeEngineStatus", resourceRoot, engine )